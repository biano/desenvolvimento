<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>ABA LOG - Atendimento</title>
	
	<!-- Font Weasome -->
	<link rel="stylesheet" href="css/fontsaw/font-awesome.css">
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
  <?php include "include/header.inc"; ?>
   <nav class="navbar navbar-default">
     <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menuprincipal">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="menuprincipal">
        <ul class="nav navbar-nav">
     <?php include "include/menu.inc"; ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href=""><i class="fa fa-unlock-alt" aria-hidden="true"></i></a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <!-- Cabeçalho do sistema-->
  <content>
    <?php include "include/reclamacoes/recList.inc"; ?>
  </content>
  		<footer>
  			<div id="footer-center" class="container">
  				<div class="row">
              
              <?php include "include/rodape.inc"; ?>

          </div>
  			</div>
  		</footer>

  		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  		<!-- Include all compiled plugins (below), or include individual files as needed -->
  		<script src="js/bootstrap.min.js"></script>
  	</body>
  	</html>