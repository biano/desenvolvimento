  <section>
   <div id="conteudo" class="container">
    <div class="row">
     <h1>Consultar Agendamentos</h1>
   </div>
 </section>
 <!-- ******************************************************************************************** -->
 <!--             PESQUISA POR DATAS: DE SOLICITAÇAO, ENCAMINHAMENTO, COLETA, ENTREGA              -->
 <!-- ******************************************************************************************** -->
 <section>
  <div class="container">
    <div class="row">
      <div class="form-group fnd-form">
        <label><h4>Pesquisa por Data</h4>
         <div class="col-md-12">
           <div class="checkbox form-inline">
            <label>
              <input type="checkbox" name="pesqData" value="solicitacao"> Por Data de Solicitação
            </label>
            <label>
              <input type="checkbox" name="pesqData" value="encaminhamento">Por Data ded Encaminhamento
            </label>
            <label>
              <input type="checkbox" name="pesqData" value="coleta">Por Data de Coleta
            </label>
            <label>
              <input type="checkbox" name="pesqData" value="entrega">Por Data de Entrega
            </label>
          </div>
        </div>
        <div class="col-md-6">
         <label for="DataInicial">Data Inicial</label>
         <input type="date" class="form-control" id="DataInicial" placeholder="Data Inicial">
       </div>
       <div class="col-md-6">
         <label for="DataInicial">Data Final</label>
         <input type="date" class="form-control" id="DataFinal" placeholder="Data Final">
       </div>
     </div>
   </label>
 </div>
</div>
</section>
<!-- ******************************************************************************************** -->
<!--        INFORME AS DATAS DA PESQUISA: cliente e serviço                                       -->
<!-- ******************************************************************************************** -->

<section>
  <div class="container">
    <div class="row">
     <div class="col-md-12">
      <div class="form-group fnd-form">
         <label for="cliente">Pesquisa por Cliente</label>
         <input type="text" class="form-control" id="cliente" placeholder="Cliente">
       </div>
    </div>
      <div class="col-md-12">
            <button type="submit" class="btn btn-default">Pesquisar</button>
            <button type="reset" class="btn btn-default">Limpar</button>
          </div>
      </div>
  </div>
</div>
</section>
<!-- fim cliente e serviço -->

<!-- ******************************************************************************************** -->
<!--                                 PESQUISA POR ROTAS, SETOR E ENTREGADOR                       -->
<!-- ******************************************************************************************** -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="form-group  fnd-form">
          <label for="servico">Pesquisa por Rota</label>
          <input type="text" class="form-control" id="rota" placeholder="Rota">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group  fnd-form">
          <label for="servico">Pesquisa por Setor</label>
          <input type="text" class="form-control" id="setor" placeholder="Setor">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group  fnd-form">
          <label for="servico">Pesquisa por Serviço</label>
          <input type="text" class="form-control" id="servico" placeholder="Serviço">
        </div>
      </div>
    </div>
  </div>
</section>

<!-- ******************************************************************************************** -->
<!--                                 PESQUISA POR STATUS, OS, CODIGO INTERNO                      -->
<!-- ******************************************************************************************** -->
<section>
  <!--inicio Pesquisa por Status Atual, O.S, Cod. Interno -->
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="form-group  fnd-form">
          <label for="servico">Estatus Atual</label>
          <input type="text" class="form-control" id="EstatusAtual" placeholder="Estatus Atual">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group  fnd-form">
          <label for="servico">O.S</label>
          <input type="text" class="form-control" id="OS" placeholder="O.S">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group  fnd-form">
          <label for="servico">Codigo Interno</label>
          <input type="text" class="form-control" id="CodigoInterno" placeholder="Codigo Interno">
        </div>
      </div>
    </div>
  </div>
  <!--Fim Pesquisa por Rota, Setor e Entregador -->
</section>

<!-- ******************************************************************************************** -->
<!--                                 PESQUISA POR STATUS, OS, CODIGO INTERNO                      -->
<!-- ******************************************************************************************** -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group fnd-form">
          <label  for="exampleInputEmail3">Solicitante/Destinatario</label>
          <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
        </div>
      </div>
      <div class="col-md-3">
        <label  for="exampleInputEmail3">CEP</label>
        <div class="form-group fnd-form">
          <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
          <label>
            <input type="checkbox"> Destino
            <!--  <input type="checkbox"> Check me out -->
          </label>
        </div>

      </div>
      <!--   <div class="col-md-3">
          <div class="form-group fnd-form">
            <label  for="exampleInputEmail3">Ordenação</label>
            <input type="text" class="form-control" id="ordenacao" placeholder="Ordenação">
          </div>
        </div> -->
        <div class="col-md-3">
          <div class="form-group fnd-form">
            <label  for="usuario">usuário</label>
            <input type="text" class="form-control" id="usuario" placeholder="usuário">
          </div>
        </div>
      </div>
    </div> 
  </section>

  <!-- ******************************************************************************************** -->
  <!--                                            ACTIONS DO FORFMULARIO                            -->
  <!-- ******************************************************************************************** -->


  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <button type="submit" class="btn btn-default">Consultar</button> 
          <button type="reset" class="btn btn-default">Limpar</button>
        </div>
        <div class="col-md-4">
         <button type="submit" class="btn btn-default">Imprimir</button> 
         <button type="submit" class="btn btn-default">Protocolos</button>

       </div>
       <div class="col-md-4">
        <button type="submit" class="btn btn-default">Gerar Arquivos Retorno</button> 
        <button type="submit" class="btn btn-default">Excel</button>

      </div>
      <!-- <div class="col-md-3"></div> -->
    </div>
  </div>
</section>
