<!-- agendamento -->
<?php

  $emailSolicitanteErr = "";

 ?>
<section>
 <div id="conteudo" class="container">
  <div class="row">
   <h1>BEM VINDO AO SISTEMA "ABA LOG!"</h1>
   <form id="formAgendamento" action="include/agendamento/AgEnvio/agendamento.php" data-toggle="validator" role="form">
     <div class="col-lg-12">
      <div class="form-group fnd-form">
        <!-- ************************************************************************************** -->
        <!--                                  PREENCHER UNIDADE                                     -->
        <!-- ************************************************************************************** -->
        <label for="selecioneUnidade"><h4>Selecione  a unidade</h4></label>
        <select name="unidadelist" class="form-control">
          <option> - Higienópolis </option>
          <option> - Jardins </option>
          <option> - Pinheiros </option>
          <option> - Faria Lima</option>
          <option> - Vila Sônia</option>
        </select>

        <label for="selecioneUnidade"><h4>Selecione  Serviço</h4></label>
        <!-- ***************************************************************************************** -->
        <!--                          INDICAR TIPO DE SERVIÇO A SER UTILIZADO                          -->
        <!-- ***************************************************************************************** -->
        <select name="servicolist" class="form-control">
          <option>Entrega por Sedex</option>
          <option>Entrega exepressa por motoboy</option>
        </select>
      </div>
    </div>
  </div>
</div>
</section>
<section>
  <!-- ********************************************************************************* -->
  <!--                            INFORMAR EMAIL DO SOLICITANTE DO SERVIÇO               -->
  <!-- ********************************************************************************* -->
  <div class="container">
    <div class="row">
     <div class="col-xs-6"> 
      <div class="form-group fnd-form">
       <label for="EmailSolicitante"><h4>Email Solicitante</h4></label>
       <input type="text" name="emailSolicitante" class="form-control" placeholder="Email do Solicitante" required>
       <span class="error">* <?php echo $emailSolicitanteErr;?></span>  
     </div>
   </div>
   <div class="col-xs-6">
    <div class="form-group fnd-form">
     <label for="custoServico"><h4>C.Custo</h4></label>
     <input type="text" name="centroCusto" class="form-control" placeholder="C. Custo" data-error="Por favor, Digite o C. Custo." required>
     <div class="help-block with-errors"></div>  
   </div>
 </div>
</div>
</div>
</section>
<!-- Email Solicitante - C.custo -->
<section id="nome-Pac-nm-Prot">
  <!-- **************************************************************************************** -->
  <!--               FORMULARIO DE AGENDAMENTO NOME SOLICITANTE E PROTOCOLO                     -->
  <!-- **************************************************************************************** -->
  <div class="container">
    <div class="row">
     <div class="col-xs-6">
      <div class="form-group fnd-form">
       <label for="nomeSolicitante"><h4>Nome do Solicitante</h4></label>
       <input type="text" name="nomeSolicitante" class="form-control" placeholder="Nome do Solicitante" data-error="Por favor, Digite o Nome do Solicitante." required>
       <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
       <div class="help-block with-errors"></div>  
     </div>
   </div>
   <div class="col-xs-6">
    <div class="form-group fnd-form">
     <label for="protocoloPrincipal"><h4>Numero do Protocolo (Protocolo Principal)</h4></label>
     <input type="text" name="protocoloPrincipal" class="form-control" placeholder="Numero do protocolo" data-error="Por favor, Digite o Numero do Protocolo." required>
     <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
     <div class="help-block with-errors"></div> 
   </div>
 </div>
</div>
</div>
</section>
<!-- Dados Solicitante -->
      <!-- <section>
        <div class="container">
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label for="protocoloSolicitante"><h4>CEP </h4></label>
                <input type="text" class="form-control" placeholder="Numero do protocolo">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label for="protocoloSolicitante"><h4>CEP Solicitante</h4></label>
                <input type="text" class="form-control" placeholder="Numero do protocolo">
              </div>
            </div>
          </div>
        </section> -->
        <section>
          <!-- *********************************************************************************** -->
          <!--                         AGENDAMENTO CEP, ENDEREÇO, NUMERO                           -->
          <!-- *********************************************************************************** -->
          <div class="container">
            <div class="row">
              <div class="col-xs-6">
                <div class="form-group fnd-form">
                  <label for="CEP"><h4>CEP</h4></label>
                  <input type="text" name="cepOrigem" class="form-control cepOrigem" placeholder="CEP de origem"
                  data-error="Por favor, Digite o CEP de Origem." required>
                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                  <div class="help-block with-errors"></div>
                  <div class="col-xs-*">
                    <label for="endereco"><h4>Endereco</h4></label>
                    <input type="text" name="enderecoOrigem" class="form-control" placeholder="Endereço de origem" data-error="Por favor, Digite o Endereço." required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>  
                  </div>
                  <div class="col-xs-*">
                    <label for="numero"><h4>Numero</h4></label>
                    <input type="text" name="numerOrigem" class="form-control" placeholder="Numero"
                    data-error="Por favor, Digite o Numero." required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                  </div>

                </div>
              </div>
              <div class="col-xs-6">
                <!-- ******************************************************************************** -->
                <!--  AGENDAMENTO CEP SOLICITANTE, ENDEREÇO SOLICITANTE, NUMERO SOLICITANTE           -->
                <!-- ******************************************************************************** -->
                <div class="form-group fnd-form"> 
                  <label for="CEPsolicitante"><h4>CEP Solicitante</h4></label>
                  <input type="text" name="cepSolicitante" class="form-control cepSolicitante" data-error="Por favor, Digite o CEP do Solicitante." required>
                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                  <div class="help-block with-errors"></div>
                  <div class="col-xs-*">
                    <label for="enderecoSolicitante"><h4>Endereco</h4></label>
                    <input type="text" name="enderecoSolicitante" class="form-control" placeholder="Endereço do Solicitante" data-error="Por favor, Digite o Endereço." required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="col-xs-*">
                    <label for="numeroSolicitante"><h4>Numero</h4></label>
                    <input type="text" name="numeroSolicitante" class="form-control" placeholder="Numero" data-error="Por favor, Digite o Numero do Solicitante." required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div class="container">
            <div class="row">
              <!-- ***************************************************************************** -->
              <!--       AGENDAMENTO NOME CONTATO OP, NUMERO CONTATO OP, OPERADORA CONTATO OP    -->
              <!-- ***************************************************************************** -->
              <div class="col-xs-6">
                <div class="form-group fnd-form"> 
                  <label for="nomeContatoOperacional"><h4>Nome Contato Operacional</h4></label>
                  <input type="text" name="nomeContatoOperacional" class="form-control" placeholder="Nome Contato Operacional" data-error="Por favor, Digite o Nome do contato OP." required>
                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                  <div class="help-block with-errors"></div>
                  <div class="col-xs-*">
                    <label for="contatoOp1"><h4>Numero Contato Operacional</h4></label>
                    <input type="text" name="numeroContatoOperacional" class="form-control telefone" class="telefone" placeholder="Numero Contato Operacional" data-error="Por favor, Digite o Numero do Contato Op."  required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div> 
                  </div>
                  <div class="col-xs-*">
                    <label for="nomeOperadoraOp1"><h4>Operadora Contato Operacional</h4></label>
                    <input type="text" name="operadoraContatoOperacional" class="form-control" placeholder="Operadora Contato Operaciona" data-error="Por favor, Digite o Nome da Operadora." required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
              </div>
              <div class="col-xs-6">
                <!-- ***************************************************************************  -->
                <!--           AGENDAMENTO NOME CONTATO SOL, NUMERO CONTATO SOL, OPERADORA SOL    -->
                <!-- ***************************************************************************  -->
                <div class="form-group fnd-form">
                  <label for="nomecontatoSolicitante"><h4>Nome Contato Solicitante</h4></label>
                  <input type="text" name="nomecontatoSolicitante" class="form-control" placeholder="Nome Contato Solicitante" data-error="Por favor, Digite o Nome do Contato." required>
                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                  <div class="help-block with-errors"></div>
                  <div class="col-xs-*">
                    <label for="NumeroContatoSolicitante"><h4>Numero Contato Solicitante</h4></label>
                    <input type="text" name="numeroContatoSolicitante" class="form-control telefone" placeholder="Numero Contato Solicitante" data-error="Por favor, Digite o Numero do Protocolo." required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div> 
                  </div>
                  <div class="col-xs-*">
                    <label for="nomeOperadoraSolicitante"><h4>Operadora</h4></label>
                    <input type="text" name="nomeOperadoraSolicitante" class="form-control" placeholder="Nome Operadora Solicitante" data-error="Por favor, Digite o Nome da Operadora." required>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div> 
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div class="container">
            <div class="row">
              <div class="organiza">
                <button type="submit" formmethod="post" class="btn btn-primary">Pesquisar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>

              </div>
            </div>
          </div>
        </section>
      </form>
    </section>
    <!-- fim agendamento -->
