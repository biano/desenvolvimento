<section>
  <div class="container">
    <div class="row">

      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Pesquisa por Data</a></li>
        <li role="presentation"><a href="#cliente" aria-controls="cliente" role="tab" data-toggle="tab">Pesquisa por Cliente</a></li>
        <li role="presentation"><a href="#servicos" aria-controls="servicos" role="tab" data-toggle="tab">Pesquisa por Serviço</a></li>
        <li role="presentation"><a href="#rota" aria-controls="rota" role="tab" data-toggle="tab">Pesquisa por Rota</a></li>
        <li role="presentation"><a href="#status" aria-controls="status" role="tab" data-toggle="tab">Pesquisa por Status</a></li>
        <li role="presentation"><a href="#outros" aria-controls="outros" role="tab" data-toggle="tab">Outras Tipos de Pesquisa</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
          <!-- ************************************************************************************** -->
          <!--        PESQUISA POR DATAS: DE SOLICITAÇAO, ENCAMINHAMENTO, COLETA, ENTREGA             -->
          <!-- *************************************************************************************  -->
          <h2>Pesquisa por Data</h2>
          <section>
            <div class="container">
              <div class="row">
                <form id="formPorData" data-toggle="validator" role="form">
                  <div class="form-group fnd-form">
                    <label><h4>Pesquisa por Data</h4>
                     <div class="col-md-12">
                       <div class="checkbox form-inline">
                        <label>
                          <input type="checkbox" name="pesqData" value="solicitacao"> Por Data de Solicitação
                        </label>
                        <label>
                          <input type="checkbox" name="pesqData" value="encaminhamento">Por Data ded Encaminhamento
                        </label>
                        <label>
                          <input type="checkbox" name="pesqData" value="coleta">Por Data de Coleta
                        </label>
                        <label>
                          <input type="checkbox" name="pesqData" value="entrega">Por Data de Entrega
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6">
                     <label for="DataInicial">Data Inicial</label>
                     <input type="date" class="form-control" id="DataInicial" placeholder="Data Inicial">

                   </div>
                   <div class="col-md-6">
                     <label for="DataInicial">Data Final</label>
                     <input type="date" class="form-control" id="DataFinal" placeholder="Data Final">

                   </div>
                 </div>
               </label>
             </div>
           </div>
         </section>
         <section>
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Pesquisar</button> 
                <button type="reset" class="btn btn-primary">Limpar</button>
              </div>
            </div>
          </div>
        </section>
      </form>
    </div>
    <div role="tabpanel" class="tab-pane" id="cliente">
      <!--*************************************************************************************** -->
      <!--            INFORME AS DATAS DA PESQUISA: cliente e serviço                             -->
      <!--*************************************************************************************** -->
      <h2>Cliente</h2>
      <section>
        <div class="container">
          <div class="row">
            <form id="formPorNome" data-toggle="validator" role="form">
             <div class="col-md-12">
              <div class="form-group fnd-form btn-tab">
               <label for="cliente">Pesquisa por Cliente</label>
               <input type="text" class="form-control" id="cliente" placeholder="Pesquisar por Cliente" type="text"  data-error="Por favor, Digite nome do Cliente. minimo 3 caracteres" data-minlength="3" required>
               <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
               <div class="help-block with-errors"></div>    
             </div>

           </div>
         </div>
       </div>
     </section>
     <section>
       <div class="container">
        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Pesquisar</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
          </div>
        </div>
      </div>
    </section>
  </form>
</div>
<div role="tabpanel" class="tab-pane" id="servicos">
  <!-- ******************************************************************************************** -->
  <!--        INFORME AS DATAS DA PESQUISA: cliente e serviço                                       -->
  <!-- ******************************************************************************************** -->
  <h2>Serviços</h2>
  <section>
    <div class="container">
      <div class="row">
        <form id="formPorNome" data-toggle="validator" role="form">
         <div class="col-md-12">
          <div class="form-group fnd-form">
            <label for="servicos">Pesquisa por Serviço</label>
            <input type="text" class="form-control" id="servico" placeholder="Pesquisar por Serviços" data-error="Por favor, Digite nome do Serviço." required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>    

          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary">Pesquisar</button>
          <button type="reset" class="btn btn-primary">Limpar</button>
        </div>
      </div>
    </div>
  </section>
</form>
</div>
<div role="tabpanel" class="tab-pane" id="rota">
  <h2>Rotas</h2>

  <!-- ******************************************************************************************** -->
  <!--                                 PESQUISA POR ROTAS, SETOR E ENTREGADOR                       -->
  <!-- ******************************************************************************************** -->
  <section>
    <div class="container">
      <div class="row">
       <form id="formPorNome" data-toggle="validator" role="form">
        <div class="col-md-4">
          <div class="form-group  fnd-form">
            <label for="servico">Pesquisa por Rota</label>
            <input type="text" class="form-control" id="rota" placeholder="Informe a Rota" data-error="Por favor, Digite a Rota." required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>    

          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group  fnd-form">
            <label for="servico">Pesquisa por Setor</label>
            <input type="text" class="form-control" id="setor" placeholder="Informe o Setor" data-error="Por favor, Digite Setor." required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>    

          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group  fnd-form">
            <label for="servico">Pesquisa por Serviço</label>
            <input type="text" class="form-control" id="servico" placeholder="Informe o Serviço" data-error="Por favor, Digite o Serviço." required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>    

          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary">Pesquisar</button>
          <button type="reset" class="btn btn-primary">Limpar</button>
        </div>
      </div>
    </div>
  </section>
</form>
</div>
<div role="tabpanel" class="tab-pane" id="status">
  <h2>Pesquisar por Status</h2>
  <!-- ******************************************************************************************** -->
  <!--                                 PESQUISA POR STATUS, OS, CODIGO INTERNO                      -->
  <!-- ******************************************************************************************** -->
  <section>
    <!--inicio Pesquisa por Status Atual, O.S, Cod. Interno -->
    <div class="container">
      <div class="row">
       <form id="formPorNome" data-toggle="validator" role="form">
        <div class="col-md-4">
          <div class="form-group  fnd-form">
            <label for="servico">Estatus Atual</label>
            <input type="text" class="form-control" id="EstatusAtual" placeholder="Estatus Atual" data-error="Por favor, Selecione o Estatus." required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>    
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group  fnd-form">
            <label for="servico">O.S</label>
            <input type="text" class="form-control" id="OS" placeholder="O.S" data-error="Por favor, Digite a OS." required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>    

          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group  fnd-form">
            <label for="servico">Codigo Interno</label>
            <input type="text" class="form-control" id="CodigoInterno" placeholder="Codigo Interno" data-error="Por favor, Digite o Codigo Interno." required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>    

          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary">Pesquisar</button>
          <button type="reset" class="btn btn-primary">Limpar</button>
        </div>
      </div>
    </div>
    <!--Fim Pesquisa por Rota, Setor e Entregador -->
  </section>
</form>
</div>
<div role="tabpanel" class="tab-pane" id="outros">
  <h2>Outras Formas de Pesquisar</h2>

  <!-- ******************************************************************************************** -->
  <!--                                 PESQUISA POR STATUS, OS, CODIGO INTERNO                      -->
  <!-- ******************************************************************************************** -->
  <section>
    <div class="container">
      <div class="row">
      <form id="formPorNome" data-toggle="validator" role="form">
        <div class="col-md-3">
            <div class="form-group fnd-form">
              <label  for="exampleInputEmail3">Solicitante/Destinatario</label>
              <input type="text" class="form-control" id="pesqPorSolDes" placeholder="Solicitante/Destinatario" data-error="Por favor, Digite Solicitante ou Destinatario." required>
              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
              <div class="help-block with-errors"></div>    
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group fnd-form">
              <label  for="exampleInputEmail3">CEP</label>
              <input type="text" class="form-control" id="pesqPorCEP" placeholder="Pesquisar por CEP" data-error="Por favor, Digite o CEP." required>
              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
              <div class="help-block with-errors"></div>   
            </div>

          </div>
          <div class="col-md-3">
            <div class="form-group fnd-form">
              <label  for="usuario">Usuário</label>
              <input type="text" class="form-control" id="usuario" placeholder="Pesquisar por Usuário" 
              data-error="Por favor, Digite o Usuário." required>
              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
              <div class="help-block with-errors"></div>   
            </div>
          </div>
        </div>
      </div> 
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Pesquisar</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
          </div>
        </div>
      </div>
    </section>
  </form>
</div>
</div>
</div>
</div>
</section>