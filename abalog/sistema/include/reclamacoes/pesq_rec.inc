<!-- ******************************************************************************************** -->
<!--                                 PESQUISA POR STATUS, OS, CODIGO INTERNO                      -->
<!-- ******************************************************************************************** -->
<section>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#protocolo" aria-controls="protocolo" role="tab" data-toggle="tab">Pesquisa por Protocolo</a></li>
          <li role="presentation"><a href="#unidade" aria-controls="unidade" role="tab" data-toggle="tab">Pesquisa por Unidade</a></li>
          <li role="presentation"><a href="#nome" aria-controls="nome" role="tab" data-toggle="tab">Pesquisar por Nome</a></li>
 <!--          <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
 -->        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="protocolo">
<!-- ******************************************************************************************** -->
<!--                                 PESQUISA POR PROTOCOLO                                       -->
<!-- ******************************************************************************************** -->
            <section>
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <h2>Pesquisar por Protocolo</h2>
                    <div class="form-group fnd-form">
                    <label  for="protocolo">Protocolo</label>
                      <input type="text" class="form-control" id="protocolo" placeholder="protocolo">
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section>
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Pesquisar</button> 
                    <button type="reset" class="btn btn-primary">Limpar</button>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div role="tabpanel" class="tab-pane" id="unidade">
            <section>
      <!--*************************************************************************************** -->
      <!--                                 PESQUISA POR UNIDADES                                  -->
      <!--*************************************************************************************** -->
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                  <h2>Pesquisar por Unidade</h2>
                    <div class="form-group fnd-form">
                      <label  for="Unidade">Unidade</label>
                      <input type="text" class="form-control" id="Unidade" placeholder="Unidade">
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section>
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Pesquisar</button> 
                    <button type="reset" class="btn btn-primary">Limpar</button>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div role="tabpanel" class="tab-pane" id="nome">
            <section>
      <!--*************************************************************************************** -->
      <!--                                 PESQUISA POR NOMES                                     -->
      <!--*************************************************************************************** -->
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2>Pesquisar por Nome</h2>
            <div class="form-group fnd-form">
              <label  for="nome">Nome</label>
              <input type="text" class="form-control" id="nome" placeholder="Pesquisar nome">
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Pesquisar</button> 
            <button type="reset" class="btn btn-primary">Limpar</button>
          </div>
        </div>
      </div>
    </section>
  </div>
<!--           <div role="tabpanel" class="tab-pane" id="settings">
            <section>
               <h2>Pesquisar por Settings</h2>
            </section>
          </div>
        -->        </div>
      </div>
    </div>
  </div>
</section>


