        <ul class="nav navbar-nav">
          <!-- Agendamentos -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Agendamento<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="default.php">Agendamentos</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="pesq_geral.php">Pesquisar Agendamentos</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="agendListExp.php">Solicitação Expressa</a></li>
            </ul>
          </li>
          <!-- Reclamações -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reclamações<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="reclamacoes.php">Registrar Reclamações</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="pesq_rec.php">Pesquisar Reclamações</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="Andreclamacoes.php">Consultar Andamento Reclamações</a></li>              
            </ul>
          </li>
          <!-- Expedição -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Expedição<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="AcompaPedidos.php">Acompanhamento de pedido</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="DetPedidos.php">Detalhes do pedido</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="OS-Pedidos.php">O.S pedido</a></li>
            </ul>
          </li>

        </ul>