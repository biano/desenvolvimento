  <section>
   <div id="conteudo" class="container">
    <div class="row">
     <h1>Usuários Cadastrados</h1>
   </div>
 </section>

 <section>
  <div class="container">
   <div class="row">
     <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">Usuários</div>

      <!-- Table -->
      <table class="table">
        <thead>
          <tr>
            <th>Nome Completo</th>
            <th>Email</th>
            <th>Data do cadastro</th>
             <th style="text-align:center">Editar</th>
            <th style="text-align:center">Apagar</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a href="#">Fulano de Tal</a></td>
            <td><a href="#">fulano@gmail.com</a></td>
            <td><a href="#">00/00/0000</a></td>
            <td align="center"><a href="#"><i class="fa fa-pencil-square fa-3" aria-hidden="true"></i></td>
            <td align="center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></td>
          </tr>
          <tr>
            <td><a href="#">Fulano Ciclano</a></td>
            <td><a href="#">Fciclano@gmail.com</a></td>
            <td><a href="#">00/00/0000</a></td>
            <td align="center"><a href="#"><i class="fa fa-pencil-square fa-3" aria-hidden="true"></i></td>
            <td align="center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></td>
          </tr>
          <tr>
            <td><a href="#">Jose Silva</a></td>
            <td><a href="#">Silva@gmail.com</a></td>
            <td><a href="#">00/00/0000</a></td>
            <td align="center"><a href="#"><i class="fa fa-pencil-square fa-3" aria-hidden="true"></i></td>
            <td align="center"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></td>
          </tr>
        </tbody>
      </table>
    </div>      
  </div>
</div>
</section>

<!-- <section>
  <div class="container">
   <div class="row">
    <div class="organiza">
     <button type="submit" class="btn btn-primary">Enviar</button><button type="submit" class="btn btn-primary">Limpar</button>
   </div>
 </div>
</div>
</section> -->