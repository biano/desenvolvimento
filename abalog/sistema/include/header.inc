

    <!--Inicio do cabeçalho do sistema -->
     <header>
      <div class="container">
        <!-- linha inicial do cabeçalho sistema  -->
        <div class="row">
          <div id="header-cabeçalho">
           <div id="icone-header"><i class="fa fa-user fa-3x"></i></div>
           <div id="saudacao-header"> <h3>Olá, <span>Fulano</span></h3></div>
           <div id="saudacao-calendar"><?php include "include/saudacao.inc" ?></div>
         </div>
       </div>
       <!-- final linha do cabeçalho -->
     </div>
   </header>