$(document).ready(function () {

    var formulario = function () {
        //Variáveis
        var _containerCPF = $('[data-container="fomulario-cpf-mascara"]');

        //Aplicando máscaras
        _containerCPF.mask('000.000.000-00', {reverse: true, placeholder: "000.000.000-00"});
    };

    //Invocando funções
    formulario();
});

